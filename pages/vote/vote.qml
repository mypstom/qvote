<js module="func" src="../../utils/time.js" ></js>
<!-- <js module="foo" src="./../utils/test.js"></js> -->
<!-- <view>{{foo.msg}}</view> -->

<view class="dashboard main-container-gray">
  <view class="user-info" qq:if="{{showInfoBox == true}}">
    <view class="info-container">
      <view class="title">請補完個人資料</view>
      <view class="label">請選擇性別</view>
      <view class="btn-group">
        <button class="{{gender == 1 ? 'active' : ''}}" bindtap="chooseSex" data-sex="1">男生</button>
        <button class="{{gender == 0 ? 'active' : ''}}" bindtap="chooseSex"  data-sex="0">女生</button>
      </view>
      <view class="label">請選擇你的分類</view>
      <view class="btn-group">
        <button bindtap="chooseSubject"  data-subject="0" class="{{subject == 0 ? 'active' : ''}}" >理工</button>
        <button  bindtap="chooseSubject"  data-subject="1" class="{{subject == 1 ? 'active' : ''}}">文科</button>
      </view>
      <button class="info-submit" bindtap="saveInfo">儲存</button>
    </view>
    
  </view>

  {{queryString.choice}}
  <view class="filter">
    <view class="item" 
    style="{{ (item.id==currentCate )? 'background:'+currentColor+'; color: white; font-weight: bold' : ''}}"
    qq:for="{{filterData}}" bindtap="changeCate" data-id="{{item.id}}" qq:key="filter-{{item.id}}">
      {{item.name}}
    </view>
  </view>
  <view class="sortType">
    <view class="sortType-item {{hot_new == 0? 'active' : ''}}" bindtap="changeTab" data-target="0">最热</view>
    <view class="text">|</view>
    <view class="sortType-item {{hot_new == 1? 'active' : ''}}" bindtap="changeTab" data-target="1">最新</view>
    <view class="text">投票、出題都可以獲得票票點數</view>
  </view>
  <view class="loading-hint" qq:if="{{voteData.length ==0}}"> 
    <image src="../../images/loading.png"></image>
    就快出來了 努力加載中！
  </view>
  <swiper duration="100" class="cards {{isChangingCard ? 'hidden' : '' }}"
    qq:if="{{voteData.length !=0}}" 
    easing-function="default" bindchange="handleCardChange" current="{{current}}">
    <block qq:for="{{currentVoteData}}" qq:key="current-{{item.id}}">
      <swiper-item class="card shadow {{currentCardIndex==index? 'active':''}} {{item.isVoted? 'voted': ''}}" >
          <view class="card-inner" style="background:{{currentColor}}">
            
            <view class="name">
              {{item.questionName}}
            </view>
            <view class="chioce">
              <button class="c-item" open-type="getUserInfo"
                qq:for="{{item.choice}}" qq:key="{{item.choice+c.id}}"
                bindtap="chooseOne" qq:for-item="c"
                data-index="{{index}}" data-question="{{item.id}}">
                <view class="whiteBg" style="transform: translateX({{item.isVoted?c.percentage: 0}}%)"
                ></view>
                <view class="c-name">
                  <view class="c-text-group">
                    <view class="c-name-text">{{c.text}}</view>
                    <view class="c-name-count" qq:if="{{item.isVoted}}">({{c.count}})</view>
                  </view>
                  <view class="percentage" qq:if="{{item.isVoted}}">
                    
                    <view qq:if="{{c.select}}" class="selected-img"><image src="../../images/selected.png"></image></view>
                    {{100+c.percentage}}%
                  </view>
                </view>
              </button>
            </view>
            <view class="card-btns">
              <view class="like" bindtap="like" data-likes="{{item.likes}}" data-id="{{item.id}}" data-index="{{index}}">
                  <image qq:if="{{!item.isLiked}}"  src="../../images/icon_heart.png"></image>
                  <image qq:if="{{item.isLiked}}"  src="../../images/icon_heart_a.png"></image>
                <text>{{item.likes}}</text>
              </view>
              <!-- <view>{{item.likes}}</view> -->
              <view class="comment" bindtap="showResults" data-index="{{index}}" data-id="{{item.id}}" data-select="{{item.selectIndex}}">
                  <image src="../../images/icon_comment.png"></image>
                  <text>{{item.comments.length}}</text>
              </view>
              <view class="share" bindtap="onShared" data-id="{{item.id}}" data-question="{{item.questionName}}" data-select="{{item.selectIndex}}">
                <button qq:if="{{item.isVoted}}" open-type="share" data-id="{{item.id}}" data-question="{{item.questionName}}" data-select="{{item.selectIndex}}">
                    <text>测测默契</text>
                    <image src="../../images/icon_share.png"></image>
                </button>
              </view>
            </view>
            <!-- <view class="mask" bindtap='fadeOutDlg' animation="{{animationBgData}}" catchtouchmove="preventTouchMove" qq:if="{{isShowed}}"></view> -->
            <view class="comment_cont {{clickMore[index]? 'isShowing':'' }} ">
              <text class="title">留言</text>
              <view class="cancelBtn" bindtap="hideResults" data-index="{{index}}">
                  <image src="../../images/icon_cancel.png"></image>
              </view>
              <view class="comment-container">
                <view class="comments">
                  <text qq:if="{{item.comments.length == 0}}">
                    真空，还没人说话呜呜呜
                  </text>
                  <image class="no_comment_img" qq:if="{{item.comments.length == 0}}" src="../../images/no_comment.png"></image>
                  <view class="com" qq:for="{{item.comments}}" qq:for-item="com" qq:for-index="comIndex" qq:key="comIndex-{{item.comments}}">
                    
                    <view class="up">
                      <text class="choice">{{item.choice[com.choice].text}} {{comIndex+1}}号</text>
                      <text class="time">{{ com.time}}</text>
                    </view>
                    <view class="msg">{{com.text}}</view>
                  </view>

                </view>
              </view>

              <view class="input_cont {{!item.isVoted ? 'bd-0' : ''}}">
                  <text qq:if="{{!item.isVoted}}">
                    快去投票，再为你的立场发声吧！
                  </text>
                  <form qq:if="{{item.isVoted}}" class="input_area" bindsubmit="sendComment">
                    <input name="msg" value="{{msg}}" placeholder="说点什么，爆出你的独家八卦" placeholder-style="color: #CCC"  bindinput='watchComment' ></input>
                    <button class="send" data-id="{{item.id}}" form-type="submit"></button>
                    <image class="send-img {{canSend ? 'op-100' : ''}}" src="../../images/send.png"></image>
                  </form>
                </view>
         
            </view>

          </view>
      </swiper-item>
    </block>
  </swiper>
</view>
 <!-- <tabbar tabbar="{{tabbar}}"></tabbar>  -->
