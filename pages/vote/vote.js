//dashboard.js
//获取应用实例
const app = getApp()
const $ = qq.createSelectorQuery()

Page({
  data: {
    // tabbar: {},
    voteData: [],
    filterData: [],
    currentVoteData: [],
    currentCate: 0,
    currentCardIndex: 0,
    currentColor: "",
    isChangingCard: false,
    current: 0,
    clickMore:[],
    gender:0,
    subject:0,
    showInfoBox: false,
    currentOpenCardId : '',
    currentOpenCardSelect: -1,
    currentOpenCardQuestion: '',
    msg:'',
    canSend: false,
    queryString: {},
    voteData_hot:[],
    hot_new: 1
  },
  onLoad: function (option) {
    qq.showTabBar();
    qq.showShareMenu({
      showShareItems: ['qq', 'qzone', 'wechatFriends', 'wechatMoment'],
      withShareTicket: true
    })
    this.loadNewContent();
    var that = this;
    
    qq.getUserInfo({
        success: function (res) {
            that.setData({
                gender: res.userInfo.gender
            })
        }
    })
    if(option.hot_new){
      that.setData({
        hot_new:option.hot_new,
        currentCate:option.cate
      })
    }
    
  },

  onShow(){
    

  },
  loadNewContent(){
    this.loadVoteData();
  },
  updateCurrentVoteByCate(){
    let _d = this.data.voteData.filter((item, index, array) => {
      item.countInArray = index;
      return item.type == this.data.currentCate;
    });


    if(this.data.hot_new==0){
      _d.sort(function(a, b){
        return b.comments.length - a.comments.length + b.likes - a.likes;
      })
    } 
    else {
      _d.sort(function(a, b){
        return b.time - a.time;
      })
    }

    this.setData({currentVoteData: _d});
    setTimeout(function(){ qq.hideLoading(); }, 1000);
  },
  chooseSubject(event){
    let subject = event.currentTarget.dataset.subject;
    this.setData({subject: subject});
  },
  chooseSex(event){
    let sex = event.currentTarget.dataset.sex;
    this.setData({gender: sex});
  },
  saveInfo(){
    //發api set userinfo
    let vm = this;
    app.globalData.sex = vm.data.gender;
    app.globalData.subject = vm.data.subject;
    qq.request({
      url: 'https://xijuzhai.com/userinfo/'+app.globalData.openid,
      data: {sex: vm.data.gender, subject: vm.data.subject},
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {
        console.log(res.data) ;
      }
    });

    this.setData({showInfoBox: false});
  },
  changeCate(event){
    let _id = event.currentTarget.dataset.id;
    if(_id != this.data.currentCate){
      this.setData({
        isChangingCard: true
      });
      setTimeout(()=>{
        this.setData({
          currentColor: this.data.filterData[_id].color,
          currentCate: _id,
          current: 0,
          currentCardIndex: 0,
          isChangingCard: false,
          clickMore:[]
        });
        this.updateCurrentVoteByCate();
      }, 300);
    }
  },
  handleCardChange(current, source){
    this.setData({currentCardIndex: current.detail.current});
  },
  showShare(){
    qq.showShareMenu({
      showShareItems: ['qq', 'qzone', 'wechatFriends', 'wechatMoment']
    })
  },
  chooseOne(event){
    if(app.globalData.openid == 0){
      //看要不要加入 網路不佳
      this.loginAction();
      return;
    };
    // if(app.globalData.sex == -1 || app.globalData.sex == undefined){
    //   console.log("請補完個人資料");
    //   // this.setData({showInfoBox: true});
    //   return;
    // }
    //哪個選項
    let index = event.currentTarget.dataset.index;

    
    let questionId = event.currentTarget.dataset.question;
    let targetVotes = this.data.currentVoteData;
    let targetVote = targetVotes.find((item, index, array)=>{
      return item.id == questionId;
    });

    let gender = this.data.gender;
    if(targetVote.isVoted) return;
    targetVote.selectIndex = index;
    targetVote.choice[index].count = parseInt(targetVote.choice[index].count)+1;
    
    //local更新百分比
    let total = 0;
    for(let i=0; i<targetVote.choice.length; i++){
      targetVote.choice[i].select = false;
      targetVote.choice[i].count = parseInt(targetVote.choice[i].count);
      total += targetVote.choice[i].count;
    }
    
    targetVote.isVoted = true;
    targetVote.choice[index].select = true;
    for(let i=0; i<targetVote.choice.length; i++){
      if(targetVote.choice[i].count == 0){
        targetVote.choice[i].percentage = -100;
      }else if(targetVote.choice[i].count == total){
        targetVote.choice[i].percentage = 0;
      }else{
        targetVote.choice[i].percentage = -(100-((targetVote.choice[i].count/total)*100).toFixed(0));
      }
    }

    //進行投票
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/updatevote/'+questionId,
      data: targetVote,
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {

        qq.request({
          url: 'https://xijuzhai.com/votelist/?voteid='+questionId+'&userid='+ app.globalData.openid,
          data: targetVote,
          header: {'content-type': 'application/json'},
          success(res) {
            console.log("更新投票紀錄完成");

            qq.showToast({
              title: '票票點數+1',
              icon: 'success',
              duration: 1500
            })
          }
        })  

      }
    })
    //更新到全部vote
    let votes = this.data.voteData;
    votes[targetVote.count] = targetVote;

    this.setData({
      currentVoteData: targetVotes,
      voteData: votes
    });
  },
  loadVoteData(){
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/votes',
      data: {},
      header: {'content-type': 'application/json'},
      success(res) {
        let temp = res.data;
        // let temp_hot = res.data;
        console.log("loadvotedata");
        for(let _d of temp){
          _d.isVoted = false;
          if(!_d.hasOwnProperty("comments")){
            _d.comments = [];
          }
        }

        for(let i in temp){
          for(let j in temp[i].comments)
          temp[i].comments[j].time = vm.timeStamp2String(temp[i].comments[j].time);
        }

        vm.setData({
          voteData: temp,
          currentCate: vm.data.currentCate,
          filterData: app.globalData.cateData,
          currentColor: app.globalData.cateData[vm.data.currentCate].color
        })
        vm.updateVoteState();
        vm.updateCurrentVoteByCate();
        
      }
    })
  },
  updateVoteState(){
    if(app.globalData.openid == 0){
      setTimeout( ()=> {this.updateVoteState()}, 500);
      return;
    }

    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/uservote/'+ app.globalData.openid,
      data: {},
      header: {'content-type': 'application/json'},
      success(res) {
        
        let temp = res.data;
        let tempVoteData = vm.data.voteData;
        for(let _d of tempVoteData){
          if( temp.voted.find( o => o.id == _d.id ) ){
            _d.isVoted = true;
          }else{
            _d.isVoted = false;
          }

          _d.currentMsg = "";
          if(_d.isVoted){
            for(let c=0; c<_d.choice.length; c++){
              if(_d.choice[c].select){
                _d.selectIndex = c;
              }
            }
          }else{
            _d.selectIndex = -1;
          }
          // if( temp.liked.find( o => o.id == _d.id ) ){
          //   _d.isLiked = true;
          // }else{
          //   _d.isLiked = false;
          // }

        }
        vm.setData({
          voteData: tempVoteData
        });
        vm.updateCurrentVoteByCate();
      }
    })
  },
  showResults(event){
    // this.loadVoteData();
    let index = event.currentTarget.dataset.index;
    let _id = event.currentTarget.dataset.id;
    let selectId = event.currentTarget.dataset.select;
    let temp = 'clickMore[' + index +']';
    
    this.setData({
      [temp] : true,
      currentOpenCardId: _id,
      currentOpenCardSelect: selectId
    })
    
  },

  hideResults(event){

    let index = event.currentTarget.dataset.index;
    let temp = 'clickMore[' + index +']'
    // newarr[0] = index;
    this.setData({
      [temp] : false,
    })
    console.log(this.data.clickMore);
  },

  like(event){
    if(app.globalData.openid == 0){
      //看要不要加入 網路不佳
      this.loginAction();
      return;
    };
    let _likes = event.currentTarget.dataset.likes;
    let _isLiked = event.currentTarget.dataset.isLiked;
    let id = event.currentTarget.dataset.id;
    let _index = event.currentTarget.dataset.index;
    
    let dataIndex = -1;
    let targetVotes = this.data.currentVoteData;

    if(targetVotes[_index].isLiked){
      targetVotes[_index].likes--;
      targetVotes[_index].isLiked = false;
      this.updateVote({likes: _likes-1, isLiked: false}, id);
    } else{
      targetVotes[_index].likes++;
      targetVotes[_index].isLiked = true;
      this.updateVote({likes: _likes+1, isLiked: true}, id);
    }
    this.setData({currentVoteData: targetVotes});
      // this.updateVoteState();
  },
  sendComment(event){
    let vId = this.data.currentOpenCardId;
    let msg = event.detail.value.msg ? event.detail.value.msg : event.detail.value ;
    let s = this.data.currentOpenCardSelect;
    console.log(s);
    // msg = JSON.stringify(msg);
    if(msg.length == 0 || msg.hasOwnProperty('msg')) return;
    let data = {text: msg, choice: s};
    console.log(data);
    qq.showLoading({
      title: '送出留言'
    })
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/addNewComment/'+ vId,
      data: data,
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {
        vm.loadVoteData();
        vm.setData({
          msg:''
        })
        qq.showToast({
          title: '票票點數+1',
          icon: 'success',
          duration: 1500
        })
      }
    });
    
  },
  watchComment(event){
    this.setData({
      canSend: (event.detail.value.length > 0)
    })
  },
  onShared(event){
    let _id = event.currentTarget.dataset.id;
    let _questionText = event.currentTarget.dataset.question;
    let _s = event.currentTarget.dataset.select;
    app.globalData.qShareid = _id;
    app.globalData.qSharename = _questionText;
    app.globalData.qSharechoice = _s;
  },
  onShareAppMessage(res) {
    if (res.from === 'button.share') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '這一題，是朋友就會對！'+res.target.dataset.question,
      path: '/pages/welcome/welcome?sendid='+app.globalData.openid+'&vid='+res.target.dataset.id+'&choice='+res.target.dataset.select+'&name='+app.globalData.userInfo.nickName,
      imageUrl: "../../images/share.png"
    }
    
  },
  updateVote(data, vId){
    console.log(data, vId);
    qq.request({
      url: 'https://xijuzhai.com/updatevote/'+ vId,
      data: data,
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {
        qq.request({
          url: 'https://xijuzhai.com/likelist/?voteid='+vId+'&userid='+ app.globalData.openid,
          header: {'content-type': 'application/json'},
          success(res) {
            console.log("更新 按愛心紀錄完成");
          }
        })  

      }
    });
  },
  checkOpenId(){
    if(app.globalData.openid != 0){
      return;
    }
  },
  changeTab(event){
    qq.showLoading({
      title: '努力加载中'
    })
    this.setData({
      hot_new: event.currentTarget.dataset.target,
      current:0,
      currentCardIndex:0
    });

    this.loadNewContent();

  },
  timeStamp2String(dateTimeStamp){
    let result;
    let minute = 1000 * 60;
    let hour = minute * 60;
    let day = hour * 24;
    let halfamonth = day * 15;
    let month = day * 30;
    let now = new Date().getTime();
    let diffValue = now - dateTimeStamp;
    if(diffValue < 0){ return; }
      let monthC = diffValue/month;
      let weekC = diffValue/(7*day);
      let dayC = diffValue/day;
      let hourC = diffValue/hour;
      let minC = diffValue/minute;
      if(monthC>=1){
      if(monthC<=12)
            result="" + parseInt(monthC) + "月前";
      else{
        result="" + parseInt(monthC/12) + "年前";
      }
    }
    else if(weekC>=1){
      result="" + parseInt(weekC) + "周前";
    }
    else if(dayC>=1){
      result=""+ parseInt(dayC) +"天前";
    }
    else if(hourC>=1){
      result=""+ parseInt(hourC) +"小时前";
    }
    else if(minC>=1){
      result=""+ parseInt(minC) +"分钟前";
    }else{
      result="刚刚";
    }
    return result;
  },
  loginAction(){
    // 登录
    let vm = this;
    qq.login({
      success(res) {
        console.log(res);
        if (res.code) {
          vm.globalData.code = res.code;
          qq.request({
            url: 'https://xijuzhai.com/openid/' + res.code,
            data: {},
            method: 'GET',
            header: {'content-type': 'application/json'},
            success(res) {
              console.log("取得user info");
              console.log(res);
              vm.globalData.openid = res.data.openid;
              vm.globalData.sex = res.data.sex;
              vm.globalData.subject = res.data.subject;

              qq.getSetting({
                success: res => {
                  if (res.authSetting['scope.userInfo']) {
                    qq.getUserInfo({
                      success: res => {
                        console.log("取得userinfo");
                        console.log(res.userInfo);
                        vm.globalData.userInfo = res.userInfo;
                        
                        let _info = res.userInfo;
                        _info.openid = vm.globalData.openid;
                                                
                        //記錄個人資料
                        qq.request({
                          url: 'https://xijuzhai.com/updateUserLibrary',
                          data: _info,
                          method: "POST",
                          header: {'content-type': 'application/json'},
                          success(res) {
                            
                          }
                        });

                      }
                    })
                  }
                }
              })

            },
            fail(err){
              console.log("取得錯誤");
              console.log(err);
            }
          })
        }
      },
      fail(err){
        console.log(err);
      }
    });
  }
})
