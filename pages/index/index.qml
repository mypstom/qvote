<view class="container">
  <view class="userinfo">
    <button wx:if="{{!hasUserInfo && canIUse}}" open-type="getUserInfo" bindgetuserinfo="getUserInfo">  </button>
    <text wx:if="{{isCheckingLogin}}">取得使用者資料中</text>
    <!-- <text wx:if="{{!hasUserInfo}}">取得使用者資料中</text>
    <text wx:if="{{!hasUserInfo && !canIUse}}">取得使用者資料中</text> -->
    <block wx:else>
      <image bindtap="bindViewTap" class="userinfo-avatar" src="{{userInfo.avatarUrl}}" mode="cover"></image>
      <text class="userinfo-nickname">{{userInfo.nickName}}</text>
    </block>
  </view>
  <view class="usermotto" wx:if="{{!isCheckingLogin}}">
    <button wx:if="{{!hasUserInfo && canIUse}}" open-type="getUserInfo" bindgetuserinfo="getUserInfo"> 請先登入 </button>
    <button wx:if="{{hasUserInfo && canIUse}}" bindtap="goDashboard"> 開始票票 </button>
  </view>
</view>
