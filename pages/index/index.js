//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: '請先登入',
    userInfo: {},
    hasUserInfo: false,
    canIUse: qq.canIUse('button.open-type.getUserInfo'),
    isCheckingLogin: false
  },
  goDashboard: function () {
    qq.reLaunch({
      url: '../vote/vote'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true,
        isCheckingLogin: false
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      this.setData({isCheckingLogin: false});
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      qq.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true,
            isCheckingLogin: false
          })
        }
      })
    }

    qq.login({
      success(res) {
        if (res.code) {
          // 登入取得 user openid
          console.log(res);
          app.globalData.code = res.code;

          qq.request({
            url: 'https://xijuzhai.com/openid/' + res.code,
            data: {},
            header: {'content-type': 'application/json'},
            success(res) {
              console.log(res);
              app.globalData.openid = res.data;
            }
          })

        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
