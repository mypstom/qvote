const app = getApp()

Page({
  onLoad: function () {
    qq.showTabBar();
    // 登录
    
  },
  onShow(){
    
    if(app.globalData.openid == 0 ){
      let vm = this;
      qq.login({
        success(res) {
          if (res.code) {
            app.globalData.code = res.code;
            qq.request({
              url: 'https://xijuzhai.com/openid/' + res.code,
              data: {},
              method: 'GET',
              header: {'content-type': 'application/json'},
              success(res) {
                console.log("取得user info");
                app.globalData.openid = res.data.openid;
                app.globalData.sex = res.data.sex;
                app.globalData.subject = res.data.subject;
                vm.getFriendlyList();
              },
              fail(err){
                console.log("取得錯誤");
                console.log(err);
              }
            })
          }
        },
        fail(err){
          console.log(err);
        }
      })
    }else{
      this.getFriendlyList();
    }
  },
  getFriendlyList(){
    this.getRankFriendsData();
  },
  getRankFriendsData: function () {
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/userLibrary',
      data: {},
      header: {'content-type': 'application/json'},
      success(res) {
        let userLibrary = res.data;
        qq.request({
          url: 'https://xijuzhai.com/friendlyList/'+ app.globalData.openid,
          data: {},
          header: {'content-type': 'application/json'},
          success(res) {
            console.log("親密度排行榜");
            console.log(res.data);
            let _r = [];
            for(let item in res.data){
              userLibrary[item].amount = res.data[item].amount;
              _r.push(userLibrary[item]);
            }
            _r = _r.sort((a, b)=>{return b.amount > a.amount? 1: -1});
            vm.setData({fd_list: _r.map(x=>{
              return {avatar: x.avatarUrl, name: x.nickName, score: x.amount}
            })});
          }
        });
      }
    });
  },
  data: {
    fd_list:[]
  }

})
