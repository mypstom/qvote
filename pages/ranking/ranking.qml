<view class="main-container-gray">
<view class="no_ranking" qq:if="{{fd_list.length == 0}}">
  <image class="bg" src="../../images/no_ranking_bg.png" mode="widthFix"></image>
  <view class="hint1">朋友每一题与你选择相同答案，默契度+5，选择不同则默契度-5</view>
  <view class="plus">+5</view>
  <view class="minus">-5</view>
  <view class="hint2">快去投票邀請朋友，測測你們的默契度吧~~~(づ｡◕‿‿◕｡)づ</view>
</view>

<view qq:if="{{fd_list.length != 0}}">
  <view class="description">
    快去邀请朋友，测测你们的默契度吧~~~(づ｡◕‿‿◕｡)づ
  </view>
  <view class="description2">
    朋友每一题与你选择相同答案，默契度+5，选择不同则默契度-5
  </view>
  <view class="fd_list">
    <view class="fd_list_item" qq:for="{{fd_list}}" qq:key="{{index}}" qq:for-item="fd">
      <view class="ranking">
        <image qq:if="{{index+1==1}}" src="../../images/cup1.png"></image>
        <image qq:if="{{index+1==2}}" src="../../images/cup2.png"></image>
        <image qq:if="{{index+1==3}}" src="../../images/cup3.png"></image>
        <text qq:if="{{index+1>3}}">{{index+1}}</text>
      </view>
      <view class="avatar"><image src="{{fd.avatar}}"></image></view>
      <view class="name">{{fd.name}}</view>
      <view class="bar {{fd.score > 0 ? 'red-bg' : ''}}">
        <view class="subBar" style="transform: translateX({{((fd.score+200)/400)*100 }}%) "></view>
                  </view>
      <view class="score {{fd.score > 0 ? 'red-color' : ''}}">{{fd.score}}</view>
    </view>
  </view>
</view>
</view>
