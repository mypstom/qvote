<view class="new-vote main-container-gray">
  <form class="myForm" bindsubmit="formSubmit" bindreset="formReset">
    <view class="card {{isShow? 'active' : ''}}">
      <view class="hint">赶快用投票找寻你的归属感吧！</view>
      <!-- <button form-type="reset" class="reset">清除表单</button> -->
      <text class="m-label">票票话题</text>
      <view class="empty"></view>
      <input placeholder="請輸入投票話題，不超過24個字" confirm-type="next"
        name="question"  value="{{question}}" placeholder-style="color: #CCC" maxlength="24" ></input>
      <view class="line"></view>
      <view class="empty"></view>
      <view class="empty"></view>
      <text class="m-label">选项<text class="small">（不超过8个字）</text></text>
      <view class="empty"></view>
      <input name="choice1" value="{{choice1}}" maxlength="8" confirm-type="next" placeholder="請輸入選項一(必填)" placeholder-style="color: #CCC"></input>
      <view class="line"></view>
      <input name="choice2" value="{{choice2}}" maxlength="8" confirm-type="next" placeholder="請輸入選項二(必填)" placeholder-style="color: #CCC"></input>
      <view class="line"></view>
      <input name="choice3" value="{{choice3}}" maxlength="8" confirm-type="next" placeholder="請輸入選項三" placeholder-style="color: #CCC"></input>
      <view class="line"></view>
      <input name="choice4" value="{{choice4}}" maxlength="8" confirm-type="next" placeholder="請輸入選項四" placeholder-style="color: #CCC"></input>
      <view class="line"></view>
      <view class="empty"></view>
      <view class="empty"></view>
      <text class="m-label">类别</text>
      <view class="empty"></view>
      <view class="filter">
        <view class="item" 
          style="{{ (item.id==currentCate )? 'background:'+currentColor+'; color: white' : 'background:#f3f3f3; color:#888'}}"
          qq:for="{{filterData}}" bindtap="changeCate" data-id="{{item.id}}">
            {{item.name}}
        </view>
      </view>
      <button form-type="submit" class="vote-it">发起票票</button>
    </view>
  </form>
</view>
<!-- <tabbar tabbar="{{tabbar}}"></tabbar> -->
