//newVote.js
//获取应用实例
const app = getApp()

Page({
  data: {
    filterData: [],
    currentColor: app.globalData.cateData[0].color,
    currentCate: 0,
    isShow: false,
    question:'',
    choice1:'',
    choice2:'',
    choice3:'',
    choice4:''
  },
  changeCate(event){
    let _id = event.currentTarget.dataset.id;
    if(_id != this.data.currentCate){
      this.setData({
        currentColor: this.data.filterData[_id].color,
        currentCate: _id
      });
    }
  },
  formReset(event){
    this.setData({currentCate: 0});
  },
  formSubmit(event){
    let vm = this;
    let formValue = event.detail.value;
    if(formValue.question == ""  || (!formValue.choice1 && !formValue.choice2)){
      qq.showToast({
        mask:true,
        title: '內容不完整',
        icon: 'none',
        duration: 2000
      });
      return;
    }
    qq.showModal({
      mask: true,
      title: '確定',
      content: '確定投票內容了嗎？送出後不能修改刪除哦！已送出的投票可以在「我的」頁面中查看。',
      success(res) {
        if (res.confirm) {
          qq.showLoading({
            title: '上傳中',
          })
          let newVote = {
            "type": vm.data.currentCate,
            "questionName": formValue.question,
            "choice": [],
            "likes": 0
          };

          if(formValue.choice1){
            newVote.choice.push({
              "text": formValue.choice1,
              "count": 0,
              "percentage": 0
            })
          }
          if(formValue.choice2){
            newVote.choice.push({
              "text": formValue.choice2,
              "count": 0,
              "percentage": 0
            })
          }
          if(formValue.choice3){
            newVote.choice.push({
              "text": formValue.choice3,
              "count": 0,
              "percentage": 0
            })
          }
          if(formValue.choice4){
            newVote.choice.push({
              "text": formValue.choice4,
              "count": 0,
              "percentage": 0
            })
          }

          qq.request({
            url: 'https://xijuzhai.com/vote/',
            data: newVote,
            method: "POST",
            header: {'content-type': 'application/json'},
            success(res) {
              console.log(res.data);
              qq.hideLoading();
              qq.showToast({
                mask:true,
                title: '發送成功',
                icon: 'success',
                duration: 1600
              });

              qq.reLaunch({
                url: '../vote/vote?hot_new=1&cate='+vm.data.currentCate
              })
              qq.request({
                url: 'https://xijuzhai.com/myvotelist/?voteid='+res.data+'&userid='+ app.globalData.openid,
                header: {'content-type': 'application/json'},
                success(res) {
                  console.log("更新發起投票紀錄完成");
                  qq.showToast({
                    title: '票票點數+2',
                    icon: 'success',
                    duration: 1500
                  })
                }
              }) 

            }
          });
          vm.setData({
            question:'',
            choice1:'',
            choice2:'',
            choice3:'',
            choice4:'',
            // currentCate: 0
            
          })

          // qq.setStorage({
          //     key: 'tab',
          //     data: 1
          //   })

        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    
  },
  onLoad: function () {
    qq.showTabBar();
    // app.editTabbar();
    this.setData({
      filterData: app.globalData.cateData
    });
    qq.showShareMenu({
      showShareItems: ['qq', 'qzone', 'wechatFriends', 'wechatMoment']
    })
  },
  onShow: function() {
    this.setData({isShow: true});
  },
  onHide: function() {
    this.setData({isShow: false});
  }
})
