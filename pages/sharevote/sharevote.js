//dashboard.js
//获取应用实例
const app = getApp()
const $ = qq.createSelectorQuery()

Page({
  data: {
    // tabbar: {},
    voteData: [],
    filterData: [],
    currentVoteData: [],
    currentCate: 0,
    currentCardIndex: 0,
    currentColor: "",
    isChangingCard: false,
    current: 0,
    clickMore:[],
    gender:0,
    subject:0,
    showInfoBox: false,
    currentOpenCardId : '',
    currentOpenCardSelect: -1,
    msg:'',
    canSend: false,
    queryString: {},
    avatarUrl: '',
    voteState: "1",
    isVoteResultShow: false
  },
  onLoad: function (option) {

    console.log(option);
    this.setData({queryString: option});

    // qq.getUserInfo({
    //   openIdList: [option.sendid],
    //   lang: 'zh_CN',
    //   success: (res) => {
    //     console.log(res.data)
    //   }
    // })

    qq.showTabBar();
    qq.showShareMenu({
      showShareItems: ['qq', 'qzone', 'wechatFriends', 'wechatMoment'],
      withShareTicket: true
    })
    this.loadNewContent();
    var that = this;
    qq.getUserInfo({
        success: function (res) {
            that.setData({
                gender: res.userInfo.gender
            })
        }
    })

    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/userLibrary',
      data: {},
      header: {'content-type': 'application/json'},
      success(res) {
        let userLibrary = res.data;
        vm.setData({avatarUrl: userLibrary[option.openid].avatarUrl})
      }
    });

  },

  
  onShow(){
    

  },
  loadNewContent(){
    qq.showLoading({
      title: '努力加载中'
    })
    this.loadVoteData();

  },
  updateCurrentVoteByCate(){
    let _d = this.data.voteData.filter((item, index, array) => {
      item.countInArray = index;
      return item.type == this.data.currentCate;
    });
    this.setData({currentVoteData: _d});
    console.log(_d);
    setTimeout(function(){ qq.hideLoading(); }, 1000);
  },
  chooseSubject(event){
    let subject = event.currentTarget.dataset.subject;
    this.setData({subject: subject});
  },
  chooseSex(event){
    let sex = event.currentTarget.dataset.sex;
    this.setData({gender: sex});
  },
  saveInfo(){
    //發api set userinfo
    let vm = this;
    app.globalData.sex = vm.data.gender;
    app.globalData.subject = vm.data.subject;
    qq.request({
      url: 'https://xijuzhai.com/userinfo/'+app.globalData.openid,
      data: {sex: vm.data.gender, subject: vm.data.subject},
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {
        console.log(res.data) ;
      }
    });

    this.setData({showInfoBox: false});
  },
  changeCate(event){
    let _id = event.currentTarget.dataset.id;
    if(_id != this.data.currentCate){
      this.setData({
        isChangingCard: true
      });
      setTimeout(()=>{
        this.setData({
          currentColor: this.data.filterData[_id].color,
          currentCate: _id,
          current: 0,
          currentCardIndex: 0,
          isChangingCard: false,
          clickMore:[]
        });
        this.updateCurrentVoteByCate();
      }, 300);
    }
  },
  handleCardChange(current, source){
    this.setData({currentCardIndex: current.detail.current});
  },
  showShare(){
    console.log("share pressed");
    qq.showShareMenu({
      showShareItems: ['qq', 'qzone', 'wechatFriends', 'wechatMoment']
    })
  },
  goToVote(){
    qq.switchTab({
      url: '/pages/ranking/ranking'
    })
  },
  chooseOne(event){
    if(this.data.isVoteResultShow){
      return;
    }

    qq.showLoading({
      title: '友情驗證中',
    })
    //哪個選項
    let index = event.currentTarget.dataset.index;
    let questionId = event.currentTarget.dataset.question;
    let targetVotes = this.data.currentVoteData;
    let targetVote = targetVotes.find((item, index, array)=>{
      return item.id == questionId;
    });
    let vm = this;
    let _amount = 0;
    if(this.data.queryString.choice == index){
      vm.updateFriendlyPoint(5);
    }else{
      vm.updateFriendlyPoint(-5);
    }


    return; //不進行任何投票

    let gender = this.data.gender;
    if(targetVote.isVoted) return;

    targetVote.choice[index].count = parseInt(targetVote.choice[index].count)+1;

    //local更新百分比
    let total = 0;
    for(let i=0; i<targetVote.choice.length; i++){
      targetVote.choice[i].select = false;
      targetVote.choice[i].count = parseInt(targetVote.choice[i].count);
      total += targetVote.choice[i].count;
    }
    
    targetVote.isVoted = true;
    targetVote.choice[index].select = true;
    for(let i=0; i<targetVote.choice.length; i++){
      if(targetVote.choice[i].count == 0){
        targetVote.choice[i].percentage = -100;
      }else if(targetVote.choice[i].count == total){
        targetVote.choice[i].percentage = 0;
      }else{
        targetVote.choice[i].percentage = -(100-((targetVote.choice[i].count/total)*100).toFixed(0));
      }
    }

    //進行投票
    qq.request({
      url: 'https://xijuzhai.com/updatevote/'+questionId,
      data: targetVote,
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {

        qq.request({
          url: 'https://xijuzhai.com/votelist/?voteid='+questionId+'&userid='+ app.globalData.openid,
          data: targetVote,
          header: {'content-type': 'application/json'},
          success(res) {
            console.log("更新投票紀錄完成");
          }
        })  

      }
    })
    //更新到全部vote
    let votes = this.data.voteData;
    votes[targetVote.count] = targetVote;

    this.setData({
      currentVoteData: targetVotes,
      voteData: votes
    });
  },
  updateFriendlyPoint(_amount){
    let vm = this;
    //送出分數
    let _d = {
      vId: this.data.queryString.vid,
      player: app.globalData.openid,
      target: this.data.queryString.sendid,
      amount: _amount
    }
    qq.request({
      url: 'https://xijuzhai.com/updateFriendly',
      data: _d,
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {
        console.log(res.data);
        console.log("友情分更新");
        qq.hideLoading();
        if(res.data.stat == 501){
          vm.setData({voteState: "3"});
        }else{
          if(_amount>0){
            //答對
            vm.setData({voteState: "2"});
          }else{
            //答錯
            vm.setData({voteState: "1"});
          }
        }
        vm.setData({isVoteResultShow: true});
      }
    });
  },
  loadVoteData(){
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/sharedvote/'+this.data.queryString.vid,
      data: {},
      header: {'content-type': 'application/json'},
      success(res) {
        let temp = res.data;
        
        temp.isVoted = false;
        if(!temp.hasOwnProperty("comments")){
          temp.comments = [];
        }
        
        let arr = [];
        arr.push(temp);

        vm.setData({
          voteData: arr,
          currentCate: temp.type,
          filterData: app.globalData.cateData,
          currentColor: app.globalData.cateData[temp.type].color
        })
        vm.updateVoteState();
        // vm.updateCurrentVoteByCate();
        
      }
    })
  },
  updateVoteState(){
    if(app.globalData.openid == 0){
      setTimeout( ()=> {this.updateVoteState()}, 500);
    };
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/uservote/'+ app.globalData.openid,
      data: {},
      header: {'content-type': 'application/json'},
      success(res) {
        
        let temp = res.data;
        let tempVoteData = vm.data.voteData;
        for(let _d of tempVoteData){

          /*
          if( temp.voted.find( o => o.id == _d.id ) ){
            _d.isVoted = true;
          }else{
            _d.isVoted = false;
          }
          */

          _d.currentMsg = "";
          if(_d.isVoted){
            for(let c=0; c<_d.choice.length; c++){
              if(_d.choice[c].select){
                _d.selectIndex = c;
              }
            }
          }else{
            _d.selectIndex = -1;
          }
          // if( temp.liked.find( o => o.id == _d.id ) ){
          //   _d.isLiked = true;
          // }else{
          //   _d.isLiked = false;
          // }

        }
        vm.setData({
          voteData: tempVoteData
        });
        vm.updateCurrentVoteByCate();
      }
    })
  },
  showResults(event){
    let index = event.currentTarget.dataset.index;
    let _id = event.currentTarget.dataset.id;
    let selectId = event.currentTarget.dataset.select;
    // var newarr = [];
    // newarr[index] = 1;
    let temp = 'clickMore[' + index +']';
    // newarr.push(index); 
    // newarr[0] = index;
    this.setData({
      [temp] : true,
      'currentOpenCardId': _id,
      'currentOpenCardSelect': selectId
    })
    
  },

  hideResults(event){

    let index = event.currentTarget.dataset.index;
    let temp = 'clickMore[' + index +']'
    // newarr[0] = index;
    this.setData({
      [temp] : false,
    })
    console.log(this.data.clickMore);
  },

  like(event){
    let _likes = event.currentTarget.dataset.likes;
    let _isLiked = event.currentTarget.dataset.isLiked;
    let id = event.currentTarget.dataset.id;
    let _index = event.currentTarget.dataset.index;
    
    let dataIndex = -1;
    let targetVotes = this.data.currentVoteData;

    if(targetVotes[_index].isLiked){
      targetVotes[_index].likes--;
      targetVotes[_index].isLiked = false;
      this.updateVote({likes: _likes-1, isLiked: false}, id);
    } else{
      targetVotes[_index].likes++;
      targetVotes[_index].isLiked = true;
      this.updateVote({likes: _likes+1, isLiked: true}, id);
    }
    this.setData({currentVoteData: targetVotes});
      // this.updateVoteState();
  },
  sendComment(event){
    let vId = this.data.currentOpenCardId;
    let msg = event.detail.value.msg ? event.detail.value.msg : event.detail.value ;
    let s = this.data.currentOpenCardSelect;
    // msg = JSON.stringify(msg);
    if(msg.length == 0 || msg.hasOwnProperty('msg')) return;
    let data = {text: msg, choice: s};
    console.log(data);
    qq.showLoading({
      title: '送出留言'
    })
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/addNewComment/'+ vId,
      data: data,
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {
        vm.loadVoteData();
        vm.setData({
          msg:''
        })
      }
    });
    
  },

  watchComment(event){
    if(event.detail.value.length > 0){
      this.setData({
          canSend: true
      })
    } else{
      this.setData({
          canSend: false
      })
      
    }
    console.log(this.data.canSend);
    console.log(event.detail.value);
  },

  onShareAppMessage(res) {
    if (res.from === 'button.share') {
      // 来自页面内转发按钮
      console.log(res.target)

    }
    return {
      title: '這一題，是朋友就會對！',
      path: '/pages/welcome/welcome?sendid=ewrwrw34242&vid=34535353534534&choice=3',
      imageUrl: "../../images/forward_img.png"
    }

  },
  updateVote(data, vId){
    console.log(data, vId);
    qq.request({
      url: 'https://xijuzhai.com/updatevote/'+ vId,
      data: data,
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {

        qq.request({
          url: 'https://xijuzhai.com/likelist/?voteid='+vId+'&userid='+ app.globalData.openid,
          header: {'content-type': 'application/json'},
          success(res) {
            console.log("更新 按愛心紀錄完成");
          }
        })  

      }
    });
  },
  checkOpenId(){
    if(app.globalData.openid != 0){
      return;
    }
  }


  

})
