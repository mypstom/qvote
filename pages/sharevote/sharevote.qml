<js module="func" src="../../utils/time.js" ></js>
<!-- <js module="foo" src="./../utils/test.js"></js> -->
<!-- <view>{{foo.msg}}</view> -->

<view class="dashboard main-container-gray" style="border-top:0;">
  <image class="bg-img" src="../../images/sharepage_bg.png"  mode="widthFix"></image>
  <view class="filter" qq:if="{{0 == 1}}">
    <view class="item" 
    style="{{ (item.id==currentCate )? 'background:'+currentColor+'; color: white; font-weight: bold' : ''}}"
    qq:for="{{filterData}}" bindtap="changeCate" data-id="{{item.id}}" qq:key="filter-{{item.id}}">
      {{item.name}}
    </view>
  </view>


  <view class="result-box">
  <!-- 答題結果區 -->
    <!-- <view class="share" >
      <button open-type="share">
          <image src="../../images/icon_share.png"></image>
      </button>
    </view> -->
  </view>

  <view class="result-pop" qq:if="{{isVoteResultShow}}">
    <image class="result-image" src="../../images/vote-state{{voteState}}.png"></image>
    <view bindtap="goToVote" class="result-btn">我也要考驗默契</view>
  </view>


  <view class="title">你有辦法跟 {{queryString.name}} 选的一样吗！？</view>
  <swiper duration="100" class="cards {{isChangingCard ? 'hidden' : '' }}" 
    easing-function="default" bindchange="handleCardChange" current="{{current}}">
    <block qq:for="{{currentVoteData}}" qq:key="current-{{item.id}}">
      <swiper-item class="card shadow {{currentCardIndex==index? 'active':''}} {{item.isVoted? 'voted': ''}}" >
          <view class="card-inner" style="background:{{currentColor}}">
            <view class="avatar"><image src="{{avatarUrl}}"></image></view>
            <view class="name">
              {{item.questionName}}
            </view>
            <view class="chioce">
              <button class="c-item" open-type="getUserInfo"
                qq:for="{{item.choice}}" qq:key="{{c.id}}"
                bindtap="chooseOne" qq:for-item="c"
                data-index="{{index}}" data-question="{{item.id}}">
                <view class="whiteBg" style="transform: translateX({{item.isVoted?c.percentage: 0}}%)"
                ></view>
                <view class="c-name">
                  <view class="c-text-group">
                    <view class="c-name-text">{{c.text}}</view>
                    <view class="c-name-count" qq:if="{{item.isVoted}}">({{c.count}})</view>
                  </view>
                  <view class="percentage" qq:if="{{item.isVoted}}">
                    
                    <view qq:if="{{c.select}}" class="selected-img"><image src="../../images/selected.png"></image></view>
                    {{100+c.percentage}}%
                  </view>
                </view>
              </button>
            </view>
            <view class="card-btns">
              <view class="like" bindtap="like" data-likes="{{item.likes}}" data-id="{{item.id}}" data-index="{{index}}">
                  <image qq:if="{{!item.isLiked}}"  src="../../images/icon_heart.png"></image>
                  <image qq:if="{{item.isLiked}}"  src="../../images/icon_heart_a.png"></image>
                <text>{{item.likes}}</text>
              </view>
              <!-- <view>{{item.likes}}</view> -->
              <view class="comment" bindtap="showResults" data-index="{{index}}" data-id="{{item.id}}" data-select="{{item.selectIndex}}">
                  <image src="../../images/icon_comment.png"></image>
                  <text>{{item.comments.length}}</text>
              </view>
              <view class="share" >
                <button qq:if="{{item.isVoted}}" open-type="share">
                    <text>测测默契</text>
                    <image src="../../images/icon_share.png"></image>
                </button>
              </view>
            </view>
            <!-- <view class="mask" bindtap='fadeOutDlg' animation="{{animationBgData}}" catchtouchmove="preventTouchMove" qq:if="{{isShowed}}"></view> -->
            <view class="comment_cont {{clickMore[index]? 'isShowing':'' }} ">
              <text class="title">留言</text>
              <view class="cancelBtn" bindtap="hideResults" data-index="{{index}}">
                  <image src="../../images/icon_cancel.png"></image>
              </view>
              <view class="comment-container">
                <view class="comments">
                  <text qq:if="{{item.comments.length == 0}}">
                    真空，还没人说话呜呜呜
                  </text>
                  <image class="no_comment_img" qq:if="{{item.comments.length == 0}}" src="../../images/no_comment.png"></image>
                  <view class="com" qq:for="{{item.comments}}" qq:for-item="com" qq:for-index="comIndex">
                    
                    <view class="up">
                      <text class="choice">{{item.choice[com.choice].text}} {{comIndex+1}}号</text>
                      <text class="time">{{ func.getDateDiff(com.time) }}</text>
                    </view>
                    <view class="msg">{{com.text}}</view>
                  </view>

                </view>
              </view>

              <view class="input_cont {{!item.isVoted ? 'bd-0' : ''}}">
                  <text qq:if="{{!item.isVoted}}">
                    快去投票，再为你的立场发声吧！
                  </text>
                  <form qq:if="{{item.isVoted}}" class="input_area" bindsubmit="sendComment">
                    <input name="msg" value="{{msg}}" placeholder="说点什么，爆出你的独家八卦" placeholder-style="color: #CCC" bindconfirm="sendComment" bindinput='watchComment' ></input>
                    <button class="send" data-id="{{item.id}}" form-type="submit"></button>
                    <image class="send-img {{canSend ? 'op-100' : ''}}" src="../../images/send.png"></image>
                  </form>
                </view>
         
            </view>

          </view>
      </swiper-item>
    </block>
  </swiper>
</view>
 <!-- <tabbar tabbar="{{tabbar}}"></tabbar>  -->
