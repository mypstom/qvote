<view class="home main-container-gray">
  <view class="user-info" qq:if="{{showInfoBox == true}}">
    <view class="info-container">
      <view class="title">請補完個人資料</view>
      <view class="label">請選擇性別</view>
      <view class="btn-group">
        <button class="{{gender == 1 ? 'active' : ''}}" bindtap="chooseSex" data-sex="1">男生</button>
        <button class="{{gender == 0 ? 'active' : ''}}" bindtap="chooseSex"  data-sex="0">女生</button>
      </view>
      <view class="label">請選擇你的分類</view>
      <view class="btn-group">
        <button bindtap="chooseSubject"  data-subject="0" class="{{subject == 0 ? 'active' : ''}}" >理工</button>
        <button  bindtap="chooseSubject"  data-subject="1" class="{{subject == 1 ? 'active' : ''}}">文科</button>
      </view>
      <button class="info-submit" bindtap="saveInfo">儲存</button>
    </view>
    
  </view>
  <view class="profile">
    <view class="user">
      <open-data class="avatar" type="userAvatarUrl"></open-data>
      <open-data class="nickname" type="userNickName"></open-data>
    </view>
    <text class="points">票票積分： {{points}}</text>
    <!-- <open-data class="gender" lang="zh_CN" type="userGender"></open-data> -->

    <!-- <text class="gender" qq:if="{{gender==0}}">女生</text>
    <text class="gender" qq:if="{{gender==1}}">男生</text>
    <text class="class" qq:if="{{subject==0}}">理工</text>
    <text class="class" qq:if="{{subject==1}}">文科</text> -->
    <!-- <image bindtap="openModify" class="modify" src="../../images/modify.png"></image> -->
  </view>

  <view class="home-tab">
    <view class="tab-item {{currentTab == 1? 'active' : ''}}" bindtap="changeTab" data-target="1">我投票的</view>
    <view class="tab-item {{currentTab == 0? 'active' : ''}}" bindtap="changeTab" data-target="0">我發佈的</view>
    <view class="tab-item {{currentTab == 2? 'active' : ''}}" bindtap="changeTab" data-target="2">積分獎勵</view>
    <view class="tab-item {{currentTab == 3? 'active' : ''}}" bindtap="changeTab" data-target="3"> ❤ 相遇票票</view>
  </view>
  <view class="tab-container">
    <view qq:if="{{currentTab == 0}}" class="tab-content">
      <text qq:if="{{myVote.length==0}}" class="empty">你還沒有發佈投票，快去發佈吧</text>
      <block qq:for="{{myVote}}" qq:key="item.id" qq:if="{{myVote.length!=0}}">
        <view class="card" bindtap="showMore" data-index="{{index}}">
          <view class="time">
            {{item.time}}
          </view>
          <view class="card-inner" style="background:{{filterData[item.type].color}}">
            <view class="name">
              {{item.questionName}}
            </view>
            <view class="chioce" qq:if="{{item.open || true}}">
              <view class="c-item"
                qq:for="{{item.choice}}"
                bindtap="chooseOne" qq:for-item="c"
                data-index="{{index}}" data-question="{{item.id}}">
                <view class="whiteBg" style="transform: translateX({{item.isVoted?c.percentage: 0}}%)"
                ></view>
                <view class="c-name">
                  <view>
                    {{c.text}}　
                  </view>
                  <view class="percentage" qq:if="{{item.isVoted}}">
                    {{100+c.percentage}}%
                  </view>
                </view>
              </view>
            </view>
            <!-- <view class="more" bindtap="" data-index="{{index}}">看最新結果 ></view> -->
          </view>
          
        </view>
      </block>
    
    </view>
    <view qq:if="{{currentTab == 1}}" class="tab-content">
      <text qq:if="{{voted.length==0}}" class="empty">你還沒有投票，快去投票吧</text>
      <block qq:for="{{voted}}" qq:if="{{voted.length != 0}}">
        <view class="card"  bindtap="showMore" data-index="{{index}}">
          <view class="time">
            {{item.time}}
          </view>
          <view class="card-inner" style="background:{{filterData[item.type].color}}">
            <view class="name">
              {{item.questionName}}
            </view>
            <view class="chioce" qq:if="{{item.open || true }}">
              <view class="c-item"
                qq:for="{{item.choice}}"
                bindtap="chooseOne" qq:for-item="c"
                data-index="{{index}}" data-question="{{item.id}}">
                <view class="whiteBg" style="transform: translateX({{item.isVoted?c.percentage: 0}}%)"
                ></view>
                <view class="c-name">
                  <view>
                    {{c.text}}　
                  </view>
                  <view class="percentage" qq:if="{{item.isVoted}}">
                    {{100+c.percentage}}%
                  </view>
                </view>
              </view>
            </view>
            <!-- <view class="more" bindtap="" data-index="{{index}}">看結果 ></view> -->
          </view>
        </view>
      </block>
    </view>

    <view qq:if="{{currentTab == 2}}" class="tab-content other-tab">
      <text class="empty">積分機制剛開放投票、發布票票都可以獲得積分哦，積分刷起來！(頁面施工中)</text>
    </view>

    <view qq:if="{{currentTab == 3}}" class="tab-content other-tab">
      <text class="empty">票票好友系統蓄勢待發(頁面施工中)</text>
    </view>

  </view>


</view>
<!-- <tabbar tabbar="{{tabbar}}"></tabbar> -->
