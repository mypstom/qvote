//home.js
//获取应用实例
const app = getApp()

Page({
  data: {
    userInfo: {},
    currentTab: 1,
    myVote: [],
    voted: [],
    filterData: [],
    gender: 0,
    subject: 0,
    showInfoBox: false,
    points: 0
  },
  onLoad: function () {
    qq.showShareMenu({
      showShareItems: ['qq', 'qzone', 'wechatFriends', 'wechatMoment']
    })
    this.loadPoints();
    this.loadVoteData();
    
  },
  onShow(){
    this.setData({gender: app.globalData.sex, subject: app.globalData.subject})
    this.loadVoteData();
    this.loadPoints();
  },saveInfo(){
    //發api set userinfo
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/userinfo/'+app.globalData.openid,
      data: {sex: vm.data.gender, subject: vm.data.subject},
      method: "POST",
      header: {'content-type': 'application/json'},
      success(res) {
        console.log(res.data) ;
      }
    })
    this.setData({showInfoBox: false});
  },
  chooseSubject(event){
    let subject = event.currentTarget.dataset.subject;
    this.setData({subject: subject});
  },
  chooseSex(event){
    let sex = event.currentTarget.dataset.sex;
    this.setData({gender: sex});
  },
  showMore(event){
    console.log(event);
    let index = event.currentTarget.dataset.index;
    let _target = this.data.myVote;

    _target[index].open = !_target[index].open;
    
    this.setData({
      myVote: _target
    });
  },
  openModify(){
    this.setData({showInfoBox: true});
  },
  changeTab(event){
    this.setData({currentTab: event.currentTarget.dataset.target});
  },
  loadVoteData(){
    if(app.globalData.openid == 0) return;
    // qq.showLoading({
    //   title: '加载個人頁面信息中',
    // })
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/uservote/'+ app.globalData.openid,
      data: {},
      header: {'content-type': 'application/json'},
      success(res) {
        let temp = res.data;


        temp.myVote.sort(function(a, b){
          return b.time - a.time;
        })

        temp.voted.sort(function(a, b){
          return b.time - a.time;
        })

        for(let key in temp.myVote){
          temp.myVote[key].time = vm.timeStamp2String(temp.myVote[key].time);
        }
        for(let key in temp.voted){
          temp.voted[key].time = vm.timeStamp2String(temp.voted[key].time);
        }
        
        vm.setData({
          myVote: temp.myVote,
          voted: temp.voted,
          filterData: app.globalData.cateData
        })
        console.log(temp)
        // qq.hideLoading();
      }
    })
  },
  // timeStamp2String (time){
  //   let datetime = new Date();
  //   datetime.setTime(time);
  //   let year = datetime.getFullYear();
  //   let month = datetime.getMonth() + 1;
  //   let date = datetime.getDate();
  //   // let hour = datetime.getHours();
  //   let hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
  //   // let minute = datetime.getMinutes();
  //   let minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
  //   let second = datetime.getSeconds();
  //   let mseconds = datetime.getMilliseconds();
  //   return year + "年" + month + "月" + date+"日 "+hour+":"+minute;
  // }

  timeStamp2String(dateTimeStamp){
    var result;
      var minute = 1000 * 60;
      var hour = minute * 60;
      var day = hour * 24;
      var halfamonth = day * 15;
      var month = day * 30;
      var now = new Date().getTime();
      var diffValue = now - dateTimeStamp;
      if(diffValue < 0){
      return;
    }
      var monthC =diffValue/month;
      var weekC =diffValue/(7*day);
      var dayC =diffValue/day;
      var hourC =diffValue/hour;
      var minC =diffValue/minute;
      if(monthC>=1){
      if(monthC<=12)
            result="" + parseInt(monthC) + "月前";
      else{
        result="" + parseInt(monthC/12) + "年前";
      }
      }
      else if(weekC>=1){
          result="" + parseInt(weekC) + "周前";
      }
      else if(dayC>=1){
          result=""+ parseInt(dayC) +"天前";
      }
      else if(hourC>=1){
          result=""+ parseInt(hourC) +"小时前";
      }
      else if(minC>=1){
          result=""+ parseInt(minC) +"分钟前";
      }else{
      result="刚刚";
    }

      return result;
  },
  loadPoints(){
    let vm = this;
    qq.request({
      url: 'https://xijuzhai.com/userpoints/' + app.globalData.openid,
      data: {},
      header: {'content-type': 'application/json'},
      success(res) {
        console.log(res.data);
        vm.setData({points: res.data.length});
      }
    })
  }

})

