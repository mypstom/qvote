//app.js
App({
  onLaunch: function (option) {

    qq.showTabBar();
    qq.showShareMenu({
      showShareItems: ['qq', 'qzone', 'wechatFriends', 'wechatMoment']
    })

    let vm = this;
    qq.getSystemInfo({
      success: function (res) {
        vm.globalData.systemInfo = res.model;
        console.log(vm.globalData.systemInfo);
      }
    });
    
    // 展示本地存储能力
    // var logs = qq.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // qq.setStorageSync('logs', logs)


    //載入公告
    qq.request({
      url: 'https://xijuzhai.com/announcement/',
      header: {'content-type': 'application/json'},
      success(res) {
        if(res.data.isShow){
          qq.showModal({
            title: '公告',
            content: res.data.content,
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      }
    })

    // 登录
    qq.login({
      success(res) {
        console.log(res);
        if (res.code) {
          vm.globalData.code = res.code;
          qq.request({
            url: 'https://xijuzhai.com/openid/' + res.code,
            data: {},
            method: 'GET',
            header: {'content-type': 'application/json'},
            success(res) {
              console.log("取得user info");
              console.log(res);
              vm.globalData.openid = res.data.openid;
              vm.globalData.sex = res.data.sex;
              vm.globalData.subject = res.data.subject;

              qq.getSetting({
                success: res => {
                  if (res.authSetting['scope.userInfo']) {
                    qq.getUserInfo({
                      success: res => {
                        console.log("取得userinfo");
                        console.log(res.userInfo);
                        vm.globalData.userInfo = res.userInfo;
                        
                        let _info = res.userInfo;
                        _info.openid = vm.globalData.openid;
                                                
                        //記錄個人資料
                        qq.request({
                          url: 'https://xijuzhai.com/updateUserLibrary',
                          data: _info,
                          method: "POST",
                          header: {'content-type': 'application/json'},
                          success(res) {
                            
                          }
                        });

                      }
                    })
                  }
                }
              })

            },
            fail(err){
              console.log("取得錯誤");
              console.log(err);
            }
          })
        }
      },
      fail(err){
        console.log(err);
      }
    });

    if(option.query.hasOwnProperty("choice")){
      setTimeout(function(){
        qq.redirectTo({
          url: '/pages/sharevote/sharevote?sendid='+option.query.sendid+'&vid='+option.query.vid+'&choice='+option.query.choice+'&name='+option.query.name
        })
      }, 1500);
    }else{
      //如果不是分享進來的
      setTimeout(function(){
        qq.switchTab({
          url: '/pages/vote/vote'
          // url: '/pages/ranking/ranking'
        })
      }, 1500);

    }

    
  },
  // editTabbar: function () {
  //   let tabbar = this.globalData.tabBar;
  //   let currentPages = getCurrentPages();
  //   let _this = currentPages[currentPages.length - 1];
  //   let pagePath = _this.route;
    
  //   (pagePath.indexOf('/') != 0) && (pagePath = '/' + pagePath);

  //   // if(pagePath.indexOf('/') != 0){
  //   //   pagePath = '/' + pagePath;
  //   // } 
   
  //   for (let i in tabbar.list) {
  //     tabbar.list[i].selected = false;
  //     (tabbar.list[i].pagePath == pagePath) && (tabbar.list[i].selected = true);
  //   }
  //   _this.setData({
  //     tabbar: tabbar
  //   });

  // },
  globalData: {
    systemInfo: null, //設備信息
    systemModel:false,
    userInfo: null,
    openid: 0,
    // tabBar: {
    //   "backgroundColor": "#ffffff",
    //   "color": "#979795",
    //   "selectedColor": "#1c1c1b",
    //   "list": [
    //     {
    //       "pagePath": "/pages/vote/vote",
    //       "iconPath": "icon/icon_home.png",
    //       "selectedIconPath": "icon/icon_home_HL.png",
    //       "text": "投票"
    //     },
    //     {
    //       "pagePath": "/pages/newVote/newVote",
    //       "iconPath": "icon/icon_release.png",
    //       "isSpecial": true,
    //       "text": " "
    //     },
    //     {
    //       "pagePath": "/pages/home/home",
    //       "iconPath": "icon/icon_mine.png",
    //       "selectedIconPath": "icon/icon_mine_HL.png",
    //       "text": "我的"
    //     }
    //   ]
    // },
    cateData: [{id:0, name:"恶搞", color: "linear-gradient(45deg, #C692F2 0%, #8643F0 100%);"},
      {id:1, name:"感情", color: "linear-gradient(45deg, #F286A0 0%, #E95F62 100%);"},
      {id:2, name:"生活", color: "linear-gradient(45deg, #8EF378 0%, #1CBBB4 100%);"},
      {id:3, name:"学习", color: "linear-gradient(45deg, #2AEEFF 0%, #5580EB 100%);"},
      {id:4, name:"食物", color: "linear-gradient(45deg, #FFD000 0%, #FFA700 100%);"}
      ]
      // {id:5, name:"其他", color: "linear-gradient(45deg, #C692F2 0%, #8643F0 100%);"}]
  }
})
