<view class="tabbar_box {{isIphoneX?'iphoneX-height':''}}" style="background-color:{{tabbar.backgroundColor}}">
  <block qq:for="{{tabbar.list}}" qq:key="{{item.pagePath}}">
    <navigator qq:if="{{item.isSpecial}}" class="tabbar_nav" hover-class="none" url="{{item.pagePath}}" style="color:{{tabbar.selectedColor}}" open-type="switchTab">
      <view class='special-wrapper'><image class="tabbar_icon" src="{{item.iconPath}}"></image></view>
      <image class='special-text-wrapper'></image>
      <text>{{item.text}}</text>
    </navigator>
    <navigator qq:else class="tabbar_nav" hover-class="none" url="{{item.pagePath}}" style="color:{{item.selected ? tabbar.selectedColor : tabbar.color}}" open-type="switchTab">
      <image class="tabbar_icon" src="{{item.selected ? item.selectedIconPath : item.iconPath}}"></image>
      <text>{{item.text}}</text>
    </navigator>
  </block>
</view>
