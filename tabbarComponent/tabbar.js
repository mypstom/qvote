// tabBarComponent/tabBar.js
const app = getApp();
Component({

  properties: {
    tabbar: {
      type: Object,
      value: {
        "backgroundColor": "#ffffff",
        "color": "#979795",
        "selectedColor": "#1c1c1b",
        "list": [
          {
            "pagePath": "pages/vote/vote",
            "iconPath": "icon/icon_home.png",
            "selectedIconPath": "icon/icon_home_HL.png",
            "text": "投票"
          },
          {
            "pagePath": "pages/newVote/newVote",
            "iconPath": "icon/icon_release.png",
            "isSpecial": true,
            "text": "　"
          },
          {
            "pagePath": "pages/home/home",
            "iconPath": "icon/icon_mine.png",
            "selectedIconPath": "icon/icon_mine_HL.png",
            "text": "我的"
          }
        ]
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isIphoneX: false
  },
  ready: function () {
    this.setData({
      isIphoneX: app.globalData.systemInfo.indexOf('iPhone X') != -1 ? true : false
    });
  },
  methods: {

  }
})